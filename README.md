# Project Title

One Paragraph of project description goes here

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

Before installing, [Node](https://nodejs.org/en/) and [NPM registry](https://www.npmjs.com/) are required.Follow [Debian and Ubuntu based distributions installing guide](https://github.com/nodesource/distributions/blob/master/README.md#debinstall)
for more information.

Make sure you have Node and NPM installed by running simple commands to see what version of each is installed and to run a simple test program:

To see if Node is installed. 
```bash
$ node -v
```
This should print a version number.

To see if NPM is installed.
```bash
$ npm -v
```
This should print NPM’s version number

Create a test file and run it. 

```bash
$ echo "console.log('Node is installed!');" > hello.js
$ node hello.js
```
This will start Node and run the code in the hello.js file. You should see the output Node is installed!.

### Installing

A step by step series of examples that tell you how to get a development env running

Say what the step will be

```
Give the example
```

And repeat

```
until finished
```

End with an example of getting some data out of the system or using it for a little demo

## Running the tests

Explain how to run the automated tests for this system

### Break down into end to end tests

Explain what these tests test and why

```
Give an example
```

### And coding style tests

Explain what these tests test and why

```
Give an example
```

## Deployment

Add additional notes about how to deploy this on a live system

## Built With

* [Vue.js](https://vuejs.org/) - The web framework used

## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](/../tags). 

## Authors

* **Francis Peuchot** - *Initial work* - @totalcake

See also the list of [contributors](/../graphs/master) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.txt](LICENSE.txt) file for details

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc


